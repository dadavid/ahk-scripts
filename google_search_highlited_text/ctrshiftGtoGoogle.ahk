﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

^+g::
    search()

search(){
    ; Google image search highlighted text
    Send, ^c
    Sleep 50
    parameter = "C:\Program Files\Google\Chrome\Application\chrome.exe" https://www.google.com/search?q="%clipboard%"
    Run %parameter%
    Return
}